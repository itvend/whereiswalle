﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WhereIsWallE
{
    public partial class Form1 : Form
    {
        List<SaveData> processesSaved = new List<SaveData>();
        List<SaveData> processesRead = new List<SaveData>();
        public Form1()
        {
            InitializeComponent();

            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "hName",
                HeaderText = "Name",
                Visible = true
            });

            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "hFileName",
                HeaderText = "FileName",
                Visible = true
            });

            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "hArgc",
                HeaderText = "Arguments",
                Visible = true
            });

            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "hX",
                HeaderText = "X Pos",
                Visible = true
            });

            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "hY",
                HeaderText = "Y Pos",
                Visible = true
            });

            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "hWidth",
                HeaderText = "Width",
                Visible = true
            });

            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "hHeight",
                HeaderText = "Height",
                Visible = true
            });

            ListOfProc();
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    PositionWindow.SetPos("Notepad", 50, 10);
        //}

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    ProcessesComboBox processesComboBox = comboBox1.SelectedItem as ProcessesComboBox;
        //    PositionWindow.SetPos(processesComboBox.IntPtr, 10, 10);
        //}

        private void ListOfProc()
        {
            Process[] processlist = Process.GetProcesses();

            List<ProcessesComboBox> processesComboBoxes = new List<ProcessesComboBox>();

            foreach (Process p in processlist)
            {
                try
                {
                    if (p.MainWindowHandle != IntPtr.Zero)
                    {
                        processesComboBoxes.Add(new ProcessesComboBox()
                        {
                            Name = p.ProcessName,
                            Id = p.Id,
                            MainWindowTitle = p.MainWindowTitle,
                            IntPtr = p.MainWindowHandle,
                            FileName = p.MainModule.FileName
                        });
                    }
                }
                catch (Win32Exception e)
                {
                    richTextBox1.AppendText(p.ProcessName + " >> " + e.Message + "\n");
                }
            }

            processesComboBoxes.Sort();

            comboBox1.DisplayMember = "FullName";
            comboBox1.ValueMember = "Self";
            comboBox1.DataSource = processesComboBoxes;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ProcessesComboBox processesComboBox = comboBox1.SelectedItem as ProcessesComboBox;
            Rectangle rect = PositionWindow.GetPos(processesComboBox.IntPtr);

            txtX.Text = rect.X.ToString();
            txtY.Text = rect.Y.ToString();

            processesSaved.Add(new SaveData()
            {
                processes = processesComboBox,
                Arguments = txtArgs.Text,
                X = rect.X,
                Y = rect.Y,
                W = rect.Size.Width,
                H = rect.Size.Height
            });

            
            DataGridViewRow dataGridViewRow = new DataGridViewRow();

            dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
            {
                Value = processesComboBox.Name
            });

            dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
            {
                Value = processesComboBox.FileName
            });

            dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
            {
                Value = txtArgs.Text
            });

            dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
            {
                Value = rect.X.ToString()
            });

            dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
            {
                Value = rect.Y.ToString()
            });

            dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
            {
                Value = rect.Size.Width.ToString()
            });

            dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
            {
                Value = rect.Size.Height.ToString()
            });

            dataGridView1.Rows.Add(dataGridViewRow);

            txtArgs.Clear();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            Xml.WriteToXmlFile("windows.xml", processesSaved);

            processesRead = Xml.ReadFromXmlFile<List<SaveData>>("windows.xml");

            foreach (SaveData sv in processesRead)
            {
                richTextBox1.AppendText($" {sv.processes.FileName} {sv.X} {sv.Y} {sv.W} {sv.H}\n");
            }
        }
        private void btnBoot_Click(object sender, EventArgs e)
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo();

            processStartInfo.WindowStyle = ProcessWindowStyle.Normal;

            processesRead = Xml.ReadFromXmlFile<List<SaveData>>("windows.xml");

            foreach(SaveData sv in processesRead)
            {
                processStartInfo.Arguments = sv.Arguments;
                processStartInfo.FileName = sv.processes.FileName;
                Process p = Process.Start(processStartInfo);

                try
                {
                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();

                    while (p.MainWindowHandle == IntPtr.Zero)
                    {
                        if(stopwatch.ElapsedMilliseconds > 2000)
                        {
                            stopwatch.Stop();
                            break;
                        }
                    }

                    PositionWindow.SetPos(p.MainWindowHandle, sv.X, sv.Y, sv.W, sv.H);

                }
                catch (Exception ex)
                {
                    if(p != null)
                    {
                        richTextBox1.AppendText(p.ProcessName + " >> " + ex.Message + "\n");
                    }
                    else
                    {
                        richTextBox1.AppendText("n/a" + " >> " + ex.Message + "\n");
                    }
                }
                
            }
        }

        private void btbReload_Click(object sender, EventArgs e)
        {
            ListOfProc();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Cleared out : " + processesSaved.Count);
            processesSaved.Clear();
            dataGridView1.Rows.Clear();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            List<SaveData> pr = Xml.ReadFromXmlFile<List<SaveData>>("windows.xml");

            processesSaved.AddRange(pr);

            foreach (SaveData sv in pr)
            {
                
                DataGridViewRow dataGridViewRow = new DataGridViewRow();

                dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = sv.processes.Name
                });

                dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = sv.processes.FileName
                });

                dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = sv.Arguments
                });

                dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = sv.X.ToString()
                });

                dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = sv.Y.ToString()
                });

                dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = sv.W.ToString()
                });

                dataGridViewRow.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = sv.H.ToString()
                });

                dataGridView1.Rows.Add(dataGridViewRow);
            }
        }

        private void btnPos_Click(object sender, EventArgs e)
        {
            ProcessesComboBox processesComboBox = comboBox1.SelectedItem as ProcessesComboBox;

            Rectangle rect = PositionWindow.GetPos(processesComboBox.IntPtr);

            richTextBox1.AppendText($"Position X : {rect.X} Position Y: {rect.Y} \n");
        }
    }

    public class SaveData
    {
        public ProcessesComboBox processes { get; set; }
        public string Arguments { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int W { get; set; }
        public int H { get; set; }
    }

    [Serializable]
    public class ProcessesComboBox : IComparable<ProcessesComboBox>
    {
        public string Name { get; set; }
        public string MainWindowTitle { get; set; }
        public string FileName { get; set; }
        public string FullName { get { return $"{Name} [{FileName}]"; } }
        [XmlIgnore]
        public int Id { get; set; }
        [XmlIgnore]
        public IntPtr IntPtr { get; set; }
        [XmlIgnore]
        public ProcessesComboBox Self { get; set; }

        public int SortByNameAscending(string name1, string name2)
        {
            return name1.CompareTo(name2);
        }

        public int CompareTo(ProcessesComboBox comparePart)
        {
            // A null value means that this object is greater.
            if (comparePart == null)
                return 1;
            else
                return this.Name.CompareTo(comparePart.Name);
        }

    }
}
