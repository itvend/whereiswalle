﻿using System;
using System.Drawing;
using System.Runtime.InteropServices; // For the P/Invoke signatures.
using System.Windows.Forms;

public static class PositionWindow
{

    // P/Invoke declarations.

    [DllImport("user32.dll", SetLastError = true)]
    static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

    [DllImport("user32.dll", SetLastError = true)]
    static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    //[DllImport("user32.dll")]
    //private static extern bool GetWindowRect(IntPtr hWnd, Rectangle rect);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int Left;        // x position of upper-left corner
        public int Top;         // y position of upper-left corner
        public int Right;       // x position of lower-right corner
        public int Bottom;      // y position of lower-right corner
    }

    const uint SWP_NOSIZE = 0x0001;
    const uint SWP_NOZORDER = 0x0004;

    public static void SetPos(String name, int x, int y)
    {
        // Find (the first-in-Z-order) Notepad window.
        IntPtr hWnd = FindWindow(name, null);

        // If found, position it.
        if (hWnd != IntPtr.Zero)
        {
            // Move the window to (0,0) without changing its size or position
            // in the Z order.
            SetWindowPos(hWnd, IntPtr.Zero, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
        }
    }

    public static void SetPos(IntPtr intPtr, int x, int y, int w, int h)
    {
        // If found, position it.
        if (intPtr != IntPtr.Zero)
        {
            // Move the window to (0,0) without changing its size or position
            // in the Z order.
            if(SetWindowPos(intPtr, IntPtr.Zero, x, y, w, h, SWP_NOZORDER) == false)
            {
                MessageBox.Show($"GetLastWin32Error() : { Marshal.GetLastWin32Error() }");
            }
        }
    }

    public static Rectangle GetPos(IntPtr intPtr)
    {
        RECT rct;

        Rectangle rect = new Rectangle();

        if (intPtr != IntPtr.Zero)
        {
            if(GetWindowRect(intPtr, out rct))
            {
                rect.X = rct.Left;
                rect.Y = rct.Top;;
                rect.Size = new Size(rct.Right - rct.Left, rct.Bottom - rct.Top);

                return rect;
            }
            else
            {
                MessageBox.Show($"GetLastWin32Error() : { Marshal.GetLastWin32Error() }");
                return rect;
            }
        }
        else
        {
            return rect;
        }
    }
}