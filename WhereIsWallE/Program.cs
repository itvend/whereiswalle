﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhereIsWallE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string [] argc)
        {
            bool ConsoleMode = false;

            for (int i = 0; i < argc.Length; i++)
            {
                if (argc[i] == "/console")
                {
                    ConsoleMode = true;
                    i++;
                }
            }
            if (ConsoleMode)
            {
                    ProcessStartInfo processStartInfo = new ProcessStartInfo();

                    processStartInfo.WindowStyle = ProcessWindowStyle.Normal;

                    List<SaveData>  processesRead = Xml.ReadFromXmlFile<List<SaveData>>("windows.xml");

                    foreach (SaveData sv in processesRead)
                    {
                        processStartInfo.Arguments = sv.Arguments;
                        processStartInfo.FileName = sv.processes.FileName;
                        Process p = Process.Start(processStartInfo);

                        try
                        {
                            Stopwatch stopwatch = new Stopwatch();
                            stopwatch.Start();

                            while (p.MainWindowHandle == IntPtr.Zero)
                            {
                                if (stopwatch.ElapsedMilliseconds > 2000)
                                {
                                    stopwatch.Stop();
                                    break;
                                }
                            }

                            PositionWindow.SetPos(p.MainWindowHandle, sv.X, sv.Y, sv.W, sv.H);

                        }
                        catch (Exception ex)
                        {
                            if (p != null)
                            {
                                Console.WriteLine(p.ProcessName + " >> " + ex.Message + "\n");
                            }
                            else
                            {
                                Console.WriteLine("n/a" + " >> " + ex.Message + "\n");
                            }
                        }

                    }
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
        }
    }
}
